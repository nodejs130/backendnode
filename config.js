const config = {
  dbUrl:
    process.env.DB_URL ||
    'mongodb+srv://admin:4py5QY49Nksud2v5P676@platzi.o2ngeot.mongodb.net/telegram',
    port: process.env.PORT || 3000,
    host: process.env.HOST || 'http://localhost',
    publicRoute: process.env.PUBLIC_ROUTE || 'app'
};

module.exports = config;

const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');

router.get('/', function (req, res) {
  controller
    .getUsers()
    .then((userList) => {
      response.success(req, res, userList, 200);
    })
    .catch((e) => {
      response.error(req, res, 'Unexpedtec error', 500, e);
    });
});

router.post('/', function (req, res) {
  const { body } = req;

  controller
    .addUser(body.name)
    .then((data) => {
      response.success(req, res, data, 201);
    })
    .catch((e) => {
      response.error(req, res, 'Internal error', 500, e);
    });
});

module.exports = router;

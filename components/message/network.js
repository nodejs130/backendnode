const express = require('express');
const multer = require('multer');

const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');

const upload = multer({
  dest: 'public/files/',
});

router.get('/', function (req, res) {
  const filterMessage = req.query.user || null;

  controller
    .getMessages(filterMessage)
    .then((messageList) => {
      response.success(req, res, messageList, 200);
    })
    .catch((e) => {
      response.error(req, res, 'Unexpected error', 500, e);
    });
});

router.post('/', upload.single('file'), function (req, res) {
  const { body, file } = req;

  controller
    .addMessage(body.chat, body.user, body.message, file)
    .then((fullMessage) => {
      response.success(req, res, fullMessage, 201);
    })
    .catch((e) => {
      response.error(
        req,
        res,
        'Informacion invalida',
        400,
        'Error en el controlador'
      );
    });
});

router.patch('/:id', function (req, res) {
  controller
    .updateMessage(req.params.id, req.body.message)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, 'Error interno', 500, e);
    });
});

router.delete('/:id', function (req, res) {
  const { params } = req;

  controller
    .deleteMessage(params.id)
    .then(() => {
      response.success(req, res, `Mensaje ${params.id} eliminado`, 200);
    })
    .catch((e) => {
      response.error(req, res, 'Error interno', 500, e);
    });
});

module.exports = router;

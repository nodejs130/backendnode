const Model = require('./model');

function addMessage(message) {
  const myMessage = new Model(message);
  myMessage.save();
}

async function getMessages(filterUser) {
  return new Promise((resolve, reject) => {
    let filter = {};

    if (filterUser != null) {
      filter = { user: filterUser };
    }

    const messages = Model.find(filter)
      .populate('user')
      .catch((e) => {
        reject(e);
      });
    resolve(messages);
  });
}

async function updateText(id, message) {
  const foundMessage = await Model.findOne({
    _id: id,
  });

  foundMessage.message = message;
  const newMessage = await foundMessage.save();

  return newMessage;
}

async function removeMessage(id) {
  return await Model.deleteOne({
    _id: id,
  });
}

module.exports = {
  add: addMessage,
  list: getMessages,
  updateText,
  remove: removeMessage,
};

const express = require('express');
const router = express.Router();

const controller = require('./controller');

const response = require('../../network/response');

router.get('/:userId', function (req, res) {
  controller
    .listChats(req.params.userId)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, 'Unexpected error', 500, e);
    });
});

router.post('/', function (req, res) {
  controller
    .addChat(req.body.users)
    .then((data) => {
      response.success(req, res, data, 201);
    })
    .catch((e) => {
      response.error(req, res, 'Unexpected error', 500, e);
    });
});

module.exports = router;

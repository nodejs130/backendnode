# Curso de Node.js: Base de Datos con MongoDB y WebSockets

Curso de [platzi](https://platzi.com/cursos/nodejs-mongo-websockets/)

## Instalación

```bash
npm i
```

## Ejecución

- **Aplicación principal:** Práctica general del curso.

```bash
node server
```

- **Aplicación socket:** Muestra de como crear y utilizar un Websocket con Express.

```bash
node index
```
